#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <sys/types.h>
//#include <OpenCL/opencl.h>
#include <unistd.h>
#include <CL/cl.h>
#include <sys/resource.h>
#include <sys/time.h>

extern double getMicroSeconds();
extern char *err_code (cl_int err_in);
extern int output_device_info(cl_device_id device_id);
extern void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_device_id *device_id, int gpucpu ,char * programCL,int numKernels,cl_kernel *kernel,char * kernelNames, ...);

double get_time(){
	static struct timeval 	tv0;
	double time_, time;

	gettimeofday(&tv0,(struct timezone*)0);
	time_=(double)((tv0.tv_usec + (tv0.tv_sec)*1000000));
	time = time_/1000000;
	return(time);
}

typedef struct { float m, x, y, z, vx, vy, vz; } body;

void randomizeBodies(body *data, int n) {
	for (int i = 0; i < n; i++) {
		data[i].m  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;

		data[i].x  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
		data[i].y  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
		data[i].z  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;

		data[i].vx = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
		data[i].vy = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
		data[i].vz = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
	}
}

void bodyForce(body *p, float dt, int n) {

	for (int i = 0; i < n; i++) { 
		float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

		for (int j = 0; j < n; j++) {
			if (i!=j) {
				float dx = p[j].x - p[i].x;
				float dy = p[j].y - p[i].y;
				float dz = p[j].z - p[i].z;
				float distSqr = dx*dx + dy*dy + dz*dz;
				float invDist = 1.0f / sqrtf(distSqr);
				float invDist3 = invDist * invDist * invDist;

				float G = 6.674e-11;
				float g_masses = G * p[j].m * p[i].m;

				Fx += g_masses * dx * invDist3; 
				Fy += g_masses * dy * invDist3; 
				Fz += g_masses * dz * invDist3;
			}
		}

		p[i].vx += dt*Fx/p[i].m; p[i].vy += dt*Fy/p[i].m; p[i].vz += dt*Fz/p[i].m;
	}
}

void integrate(body *p, float dt, int n){
	for (int i = 0 ; i < n; i++) {
		p[i].x += p[i].vx*dt;
		p[i].y += p[i].vy*dt;
		p[i].z += p[i].vz*dt;
	}
}

void printBody(body *p,int nBodies){

	for (int i=0;i<nBodies;i++){
		printf ("%f , %f , %f , %f , %f , %f , %f \n",p[i].m, p[i].x,p[i].y,p[i].z,p[i].vx,p[i].vy,p[i].vz);	
	}




}
void copia(body *d_p, body *p,int nBodies){

	for (int i=0;i<nBodies;i++){
		d_p[i].m = p[i].m;
		d_p[i].x = p[i].x;
		d_p[i].y = p[i].y;
		d_p[i].z = p[i].z;
		d_p[i].vx = p[i].vx;
		d_p[i].vy = p[i].vy;
		d_p[i].vz = p[i].vz;
	}


}

void ejecutaNbodyEnGPU(int gpucpu,int nBodies,body *d_p,float dt,int nIters){

	cl_context context; 
	cl_command_queue command_queue;
	cl_program program;       // compute program
	cl_kernel kernel[2];
	cl_device_id device_id;
	cl_mem d_body;
	size_t global[2];  
	int err;
	 double t1 = get_time();
	//inicializaGPU(&context, &command_queue,&program,&kernel);
	inicializaGPU(&context ,&command_queue,&program,&device_id,gpucpu,"nbody.cl",2,kernel,"bodyForce","integrate");


	d_body = clCreateBuffer(context,  CL_MEM_READ_WRITE, nBodies*sizeof(body), NULL, &err);


	if(clEnqueueWriteBuffer(command_queue, d_body, CL_TRUE, 0, nBodies*sizeof(body),d_p, 0, NULL, NULL)!=CL_SUCCESS){
		printf("Problemas al transferir Tex!\n");
		exit(1);
	}
	



	if (clSetKernelArg(kernel[0], 0, sizeof(cl_mem), &d_body) != CL_SUCCESS)
		printf("ha fallado el setKernelArg[1] 0\n");
	if (clSetKernelArg(kernel[0], 1, sizeof(cl_float), &dt)!= CL_SUCCESS)
		printf("a fallado el setKernelArg[1] 1\n");
	if (clSetKernelArg(kernel[0], 2, sizeof(cl_int), &nBodies)!= CL_SUCCESS)
		printf("a fallado el setKernelArg[1] 1\n");

	if (clSetKernelArg(kernel[1], 0, sizeof(cl_mem), &d_body) != CL_SUCCESS)
		printf("ha fallado el setKernelArg[1] 0\n");
	if (clSetKernelArg(kernel[1], 1, sizeof(cl_float), &dt)!= CL_SUCCESS)
		printf("a fallado el setKernelArg[1] 1\n");
	if (clSetKernelArg(kernel[1], 2, sizeof(cl_int), &nBodies)!= CL_SUCCESS)
		printf("a fallado el setKernelArg[1] 1\n");

	global[0] = nBodies;
	global[1] = nBodies;


	for (int iter = 1; iter <= nIters; iter++) {
		err=clEnqueueNDRangeKernel(command_queue, kernel[0], 1, NULL, global, NULL,0, NULL, NULL);		
		clFinish(command_queue);		
		err=clEnqueueNDRangeKernel(command_queue, kernel[1], 1, NULL, global, NULL,0, NULL, NULL);
		clFinish(command_queue);		
	}

	clFinish(command_queue);	

	if (clEnqueueReadBuffer( command_queue, d_body, CL_TRUE, 0, nBodies*sizeof(body), d_p , 0, NULL, NULL )!=CL_SUCCESS)
	 	printf("ha fallado la lectura de d_body\n");
	
	//printf("d_p ya calculado\n");
	//printBody(d_p,nBodies);

	

	
	clReleaseMemObject(d_body);   
    clReleaseProgram(program);
    clReleaseKernel(kernel[0]);
    clReleaseKernel(kernel[1]);   
    clReleaseCommandQueue(command_queue);
    clReleaseContext(context);


    //ahora toca hacerlo como una estructura con arrays
    //es decir, hacer malloc de todas las m juntas luego todas las x juntas, las y juntas, etc etc etc

 
	
	double totalTimeGPU = get_time()-t1; 
	printf("\n%d Bodies with %d iterations: %0.9f Millions Interactions/second\n", nBodies, nIters, 1e-6 * nBodies * nBodies / totalTimeGPU);


}

int main(const int argc, const char** argv) {

	int nBodies = 1000;
	cl_int err;
	             // global domain size
	if (argc > 1) nBodies = atoi(argv[1]);

	const float dt = 0.01f; // time step
	const int nIters = 100;  // simulation iterations
	

	body *p = (body*)malloc(nBodies*sizeof(body));
	body *d_p = (body*)malloc(nBodies*sizeof(body));
	randomizeBodies(p, nBodies); // Init pos / vel data
	copia(d_p,p,nBodies);


	double t0 = get_time();

	for (int iter = 1; iter <= nIters; iter++) {
		bodyForce(p, dt, nBodies); // compute interbody forces
		integrate(p, dt, nBodies); // integrate position
	}


	printf("---------------------------CPU-------------------------------------\n");
	double totalTime = get_time()-t0; 
	printf("%d Bodies with %d iterations: %0.9f Millions Interactions/second \n", nBodies, nIters, 1e-6 * nBodies * nBodies / totalTime);


	

  
	printf("---------------------------GPU de la CPU-------------------------------------\n");
    ejecutaNbodyEnGPU(0,nBodies,d_p,dt,nIters); //GPU de la  CPU
    printf("---------------------------GPU-------------------------------------\n");
    ejecutaNbodyEnGPU(1,nBodies,d_p,dt,nIters); //GPU
    free(d_p);
    free(p);
	
 	}


