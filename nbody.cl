typedef struct { float m, x, y, z, vx, vy, vz; } body;

__kernel void bodyForce(__global body *p, float dt, int n) {
	int i = get_global_id(0);
	if (i<n) { 
		float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

		for (int j=0;j<n;j++) {
			if (i!=j) {
				float dx = p[j].x - p[i].x;
				float dy = p[j].y - p[i].y;
				float dz = p[j].z - p[i].z;
				float distSqr = dx*dx + dy*dy + dz*dz;
				float invDist = 1.0f / sqrt(distSqr);
				float invDist3 = invDist * invDist * invDist;

				float G = 6.674e-11;
				float g_masses = G * p[j].m * p[i].m;

				Fx += g_masses * dx * invDist3; 
				Fy += g_masses * dy * invDist3; 
				Fz += g_masses * dz * invDist3;
			}
		}

		p[i].vx += dt*Fx/p[i].m; p[i].vy += dt*Fy/p[i].m; p[i].vz += dt*Fz/p[i].m;
	}
}

__kernel void integrate(__global body *p, float dt, int n){
	int i= get_global_id(0);
	if (i<n){
		p[i].x += p[i].vx*dt;
		p[i].y += p[i].vy*dt;
		p[i].z += p[i].vz*dt;
	}
}